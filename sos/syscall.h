/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_SYSCALL_H_
#define _SOS_SYSCALL_H_

/**
 * @file syscall.h
 *
 * SOS Syscalls identifiers and handler. The handler is called by the
 * underlying assembler routine (see hwcore/syscall.h)
 */


/**
 * The user services offered by the SOS kernel
 */
#define SOS_SYSCALL_ID_EXIT        256 /**< Args: retval, retval=NONE */

#define SOS_SYSCALL_ID_BOCHS_WRITE 43  /**< Args: string, retval=num_printed */


#if defined(KERNEL_SOS) && !defined(ASM_SOURCE)

#include <sos/errno.h>
#include <hwcore/cpu_context.h>

/**
 * "The" SOS syscall handler
 *
 * @param syscall_id The identifier of the syscall service
 * @param user_ctxt The user context making the syscall
 */
sos_ret_t sos_do_syscall(int syscall_id,
			 const struct sos_cpu_state *user_ctxt);

#endif /* defined(KERNEL_SOS) && !defined(ASM_SOURCE) */

#endif /* _SOS_SYSCALL_H_ */
