/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/

#include <hwcore/paging.h>
#include <sos/thread.h>
#include <sos/assert.h>
#include <sos/kmalloc.h>

#include "uaccess.h"


/**
 * Helper function to force the MMU to switch to the address space of
 * the process
 */
static sos_ret_t bindto_user_mm_context()
{
  struct sos_thread * current_thread;
  struct sos_mm_context * process_mm_ctxt;

  current_thread = sos_thread_get_current();
  SOS_ASSERT_FATAL(NULL != current_thread->process);

  /* Switch to the user's address space */
  process_mm_ctxt
    = sos_process_get_mm_context(current_thread->process);
  return sos_thread_change_current_mm_context(process_mm_ctxt);
}


/**
 * Helper macro to restore the MMU configuration prior to
 * bindto_user_mm_context()
 */
#define unbindto_user_mm_context() \
   sos_thread_change_current_mm_context(NULL)


static sos_ret_t nocheck_user_memcpy(sos_vaddr_t dest,
				     sos_vaddr_t src,
				     sos_size_t size,
				     sos_bool_t transfer_from_user)
{
  sos_ret_t retval;

  retval = bindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  memcpy((void*) dest, (void*) src, size);

  retval = unbindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  return size;
}


sos_ret_t sos_memcpy_from_user(sos_vaddr_t kernel_to,
			       sos_uaddr_t user_from,
			       sos_size_t size)
{
  /* Make sure user is trying to access user space */
  if (user_from < SOS_PAGING_BASE_USER_ADDRESS)
    return -SOS_EPERM;

  if (user_from > SOS_PAGING_TOP_USER_ADDRESS - size)
    return -SOS_EPERM;

  return nocheck_user_memcpy(kernel_to, user_from, size, TRUE);
}


sos_ret_t sos_memcpy_to_user(sos_uaddr_t user_to,
			     sos_vaddr_t kernel_from,
			     sos_size_t size)
{
  /* Make sure user is trying to access user space */
  if (user_to < SOS_PAGING_BASE_USER_ADDRESS)
    return -SOS_EPERM;

  if (user_to > SOS_PAGING_TOP_USER_ADDRESS - size)
    return -SOS_EPERM;

  return nocheck_user_memcpy(user_to, kernel_from, size, FALSE);
}


sos_ret_t sos_strnlen_from_user(sos_uaddr_t user_str, sos_size_t max_len)
{
  sos_ret_t retval, len;

  /* Make sure user is trying to access user space */
  if (user_str < SOS_PAGING_BASE_USER_ADDRESS)
    return -SOS_EPERM;

  /* Don't allow invalid max_len */
  if ( (max_len < 1) || (max_len > SOS_PAGING_USER_SPACE_SIZE) )
    return -SOS_EINVAL;

  retval = bindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  len = strnlen((const char*)user_str, max_len);

  retval = unbindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  return len;
}


static sos_ret_t nocheck_user_strzcpy(char *dst, const char *src,
				      sos_size_t len)
{
  sos_ret_t retval;

  retval = bindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  strzcpy(dst, src, len);

  retval = unbindto_user_mm_context();
  if (SOS_OK != retval)
    return retval;

  return SOS_OK;
}


sos_ret_t sos_strzcpy_from_user(char *kernel_to, sos_uaddr_t user_from,
				sos_size_t max_len)
{
  /* Make sure user is trying to access user space */
  if ((sos_uaddr_t)user_from < SOS_PAGING_BASE_USER_ADDRESS)
    return -SOS_EPERM;

  /* Don't allow invalid max_len */
  if ( (max_len < 1) || (max_len > SOS_PAGING_USER_SPACE_SIZE) )
    return -SOS_EINVAL;

  return nocheck_user_strzcpy(kernel_to, (const char*)user_from, max_len);
}


sos_ret_t sos_strzcpy_to_user(sos_uaddr_t user_to, const char *kernel_from,
			      sos_size_t max_len)
{
  /* Make sure user is trying to access user space */
  if ((sos_uaddr_t)user_to < SOS_PAGING_BASE_USER_ADDRESS)
    return -SOS_EPERM;

  /* Don't allow invalid max_len */
  if ( (max_len < 1) || (max_len > SOS_PAGING_USER_SPACE_SIZE) )
    return -SOS_EINVAL;

  return nocheck_user_strzcpy((char*)user_to, kernel_from, max_len);
}


sos_ret_t sos_strndup_from_user(char ** kernel_to, sos_uaddr_t from_user,
				sos_size_t max_len,
				sos_ui32_t kmalloc_flags)
{
  sos_ret_t retval = sos_strnlen_from_user(from_user, max_len);
  if (retval < 0)
    return retval;

  *kernel_to = (char*)sos_kmalloc(retval + 1, kmalloc_flags);
  if (NULL == *kernel_to)
    return -SOS_ENOMEM;

  retval = sos_strzcpy_from_user(*kernel_to, from_user, retval + 1);
  if (SOS_OK != retval)
    {
      sos_kfree((sos_vaddr_t)*kernel_to);
      *kernel_to = NULL;
    }

  return retval;
}
