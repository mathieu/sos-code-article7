/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_UACCESS_H_
#define _SOS_UACCESS_H_


/**
 * @file uaccess.h
 *
 * Routines to access user-space data from inside the kernel space.
 */

#include <sos/types.h>
#include <sos/errno.h>


/**
 * Retrieve a bunch of data from the user space of the
 * current_thread->process
 *
 * @return <0 on error ! Return the number of bytes successfully copied
 * otherwise.
 */
sos_ret_t sos_memcpy_from_user(sos_vaddr_t kernel_to,
			       sos_uaddr_t user_from,
			       sos_size_t size);


/**
 * Transfer a bunch of data to the user space of the
 * current_thread->process
 *
 * @return <0 n error ! Return the number of bytes successfully copied
 * otherwise.
 */
sos_ret_t sos_memcpy_to_user(sos_uaddr_t user_to,
			     sos_vaddr_t kernel_from,
			     sos_size_t size);


/**
 * @return the length of the given user space string user_str
 * (excluding the trailing \0), up to max_len bytes. <0 on error
 * (unresolved page fault, etc.)
 */
sos_ret_t sos_strnlen_from_user(sos_uaddr_t user_str, sos_size_t max_len);


/**
 * Copy the given user space string to kernel space, up to max_len
 * bytes (including trailing \0)
 *
 * @return SOS_OK on success, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strzcpy_from_user(char *kernel_to, sos_uaddr_t user_from,
				sos_size_t max_len);


/**
 * Copy the given kernel string to user space, up to max_len bytes
 * (including trailing \0)
 *
 * @return SOS_OK on success, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strzcpy_to_user(sos_uaddr_t user_to, const char *kernel_from,
			      sos_size_t max_len);


/**
 * Copy the given user space string into a new allocated kernel space
 * area of the correct size, up to max_len bytes (including trailing
 * \0)
 *
 * @return SOS_OK on success + *kernel_to is set to the freshly
 * allocated kernel string, <0 otherwise (unresolved page fault, etc.)
 */
sos_ret_t sos_strndup_from_user(char ** kernel_to, sos_uaddr_t from_user,
				sos_size_t max_len,
				sos_ui32_t kmalloc_flags);

#endif /* _SOS_UACCESS_H_ */
