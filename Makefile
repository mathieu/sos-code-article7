## Copyright (C) 2004,2005  The SOS Team
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
## USA. 

CC=gcc
LD=ld
CFLAGS  = -Wall -nostdinc -ffreestanding -DKERNEL_SOS -m32	\
	  -fno-asynchronous-unwind-tables -fno-stack-protector
LIBGCC := $(shell $(CC) -print-libgcc-file-name -m32) # To benefit from FP/64bits artihm.
LDFLAGS = --warn-common -nostdlib -m elf_i386
OBJECTS = bootstrap/multiboot.o					\
          hwcore/idt.o hwcore/gdt.o				\
	  hwcore/swintr.o hwcore/swintr_wrappers.o		\
	  hwcore/exception.o hwcore/exception_wrappers.o	\
	  hwcore/irq.o hwcore/irq_wrappers.o hwcore/i8259.o	\
	  hwcore/paging.o 			                \
	  hwcore/i8254.o drivers/x86_videomem.o drivers/bochs.o	\
	  hwcore/cpu_context.o hwcore/cpu_context_switch.o	\
	  hwcore/mm_context.o					\
	  sos/kmem_vmm.o sos/kmem_slab.o sos/kmalloc.o		\
	  sos/physmem.o sos/klibc.o				\
	  sos/thread.o sos/kwaitq.o				\
          sos/time.o sos/sched.o sos/ksynch.o			\
	  sos/process.o sos/syscall.o				\
          sos/assert.o sos/main.o sos/mouse_sim.o               \
	  sos/uaccess.o sos/calcload.o				\
          userland/userprogs.kimg sos/test-art7.o

KERNEL_OBJ   = sos.elf
MULTIBOOT_IMAGE = fd.img
PWD := $(shell pwd)

# Main target
all: $(MULTIBOOT_IMAGE)

$(MULTIBOOT_IMAGE): $(KERNEL_OBJ)
	./support/build_image.sh $@ $<

$(KERNEL_OBJ): $(OBJECTS) ./support/sos.lds
	$(LD) $(LDFLAGS) -T ./support/sos.lds -o $@ $(OBJECTS) $(LIBGCC)
	-nm -C $@ | cut -d ' ' -f 1,3 > sos.map
	size $@

-include .mkvars

# Create the userland programs to include in the kernel image
userland/userprogs.kimg: FORCE
	$(MAKE) -C userland

# Create objects from C source code
%.o: %.c
	$(CC) -I$(PWD) -c $< $(CFLAGS) -o $@

# Create objects from assembler (.S) source code
%.o: %.S
	$(CC) -I$(PWD) -c $< $(CFLAGS) -DASM_SOURCE=1 -o $@

FORCE:
	@

# Clean directory
clean:
	$(RM) *.img *.o mtoolsrc *~ menu.txt *.img *.elf *.bin *.map
	$(RM) *.log *.out bochs*
	$(RM) bootstrap/*.o bootstrap/*~
	$(RM) drivers/*.o drivers/*~
	$(RM) hwcore/*.o hwcore/*~
	$(RM) sos/*.o sos/*~
	$(RM) support/*~
	$(RM) extra/*~
	$(MAKE) -C userland clean
