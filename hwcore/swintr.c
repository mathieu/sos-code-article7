/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#include <sos/uaccess.h>

#include "idt.h"
#include "irq.h"

#include "swintr.h"

/**
 * The Assembler low-level wrapper for the SOS syscalls
 *
 * This wrapper takes care of saving the state of the calling user
 * thread before calling the "C" function sos_do_syscall(), passing it
 * the correct arguments.
 */
extern void sos_syscall_wrapper();


sos_ret_t sos_swintr_subsystem_setup(void)
{
  sos_ui32_t flags;
  sos_ret_t retval;

  sos_disable_IRQs(flags);

  retval
    = sos_idt_set_handler(SOS_SWINTR_SOS_SYSCALL,
			  (sos_vaddr_t) sos_syscall_wrapper,
			  3 /* CPL3 routine */);

  sos_restore_IRQs(flags);

  return retval;
}
