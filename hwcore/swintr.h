/* Copyright (C) 2005  David Decotigny

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_SWINTR_H_
#define _SOS_SWINTR_H_

/**
 * @file swintr.h
 *
 * SOS handlers for the software interrupts. In SOS, we handle a
 * single software interrupt: the syscall interrupt.
 */


/**
 * The SOS software interrupt number for issueing syscalls
 */
#define SOS_SWINTR_SOS_SYSCALL  0x42


#if defined(KERNEL_SOS) && !defined(ASM_SOURCE)

#include <hwcore/cpu_context.h>
#include <sos/errno.h>

sos_ret_t sos_swintr_subsystem_setup(void);

#endif /* defined(KERNEL_SOS) && !defined(ASM_SOURCE) */

#endif /* _SOS_SWINTR_H_ */
