/* Copyright (C) 2003  The KOS Team
   Copyright (C) 1999  Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_LIBC_STRING_H_
#define _SOS_LIBC_STRING_H_

#include <types.h>

/**
 * @file string.h
 *
 * Most of the prototypes of these functions are borrowed from
 * FreeBSD, but their implementation (in klibc.c) come either from Kos
 * (GPL v2) or from David Decotigny (SOS).
 */

void *memcpy(void *dst, const void *src, register unsigned int size ) ;
void *memset(void *dst, register int c, register unsigned int length ) ;
int memcmp(const void *s1, const void *s2, size_t n);

unsigned int strlen( register const char *str) ;
unsigned int strnlen(const char * s, size_t maxlen);

/**
 * @note Same as strncpy(), with a slightly different semantic.
 * Actually, strncpy(3C) says " The result will not be null-terminated
 * if the length of 'from' is n or more.".  Here, 'dst' is ALWAYS
 * null-terminated. And its total len will ALWAYS be <= len, with
 * null-terminating-char included.
 */
char *strzcpy( register char *dst, register const char *src,
               register int len ) ;

/**
 * @note Same as strncat(), with the same semantic : 'dst' is ALWAYS
 * null-terminated. And its total len will ALWAYS be <= len, with
 * null-terminating-char included.
 */
char *strzcat (char *dest, const char *src,
               const size_t len);
 
int strcmp(register const char *s1, register const char *s2 );
int strncmp(register const char *s1, register const char *s2,
	    register int len );

#endif /* _SOS_LIBC_STRING_H_ */
