/* Copyright (C) 2005 David Decotigny
   Copyright (C) 2003 Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_USER_CRT_H_
#define _SOS_USER_CRT_H_


/**
 * @file crt.h
 *
 * C runtime environment for the user side of the kernel/user
 * interface
 */

#include <types.h>

/**
 * We include the syscall.h file from the kernel to retrieve the list
 * of available syscall ID
 */
#include <sos/syscall.h>


/**
 * The syscall wrappers hiding the details of the implementation
 */
int _sos_syscall0(int id);

int _sos_syscall1(int id,
		  unsigned int arg1);

int _sos_syscall2(int id,
		  unsigned int arg1,
		  unsigned int arg2);

int _sos_syscall3(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3);

int _sos_syscall4(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4);

int _sos_syscall5(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5);

int _sos_syscall6(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6);

int _sos_syscall7(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6,
		  unsigned int arg7);

int _sos_syscall8(int id,
		  unsigned int arg1,
		  unsigned int arg2,
		  unsigned int arg3,
		  unsigned int arg4,
		  unsigned int arg5,
		  unsigned int arg6,
		  unsigned int arg7,
		  unsigned int arg8);


/**
 * The most important function of a C program ! ;)
 */
void _sos_exit (int status) __attribute__((noreturn));


/**
 * Non standard function to print something on bochs
 */
int _sos_bochs_write(const char *str, unsigned length);
#endif /* _SOS_USER_CRT_H_ */
