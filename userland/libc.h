/* Copyright (C) 2005 David Decotigny
   Copyright (C) 2003 Thomas Petazzoni

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA. 
*/
#ifndef _SOS_USER_LIBC_H_
#define _SOS_USER_LIBC_H_

#include <types.h>

/**
 * @file libc.h
 *
 * The basic user C library for user programs
 */

/**
 * The most important function of a C program ! ;)
 */
void exit (int status) __attribute__((noreturn, alias("_sos_exit")));

#endif /* _SOS_USER_LIBC_H_ */
